package cs.mad.flashcards.adapters

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardDao
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class FlashcardAdapter(dataSet: List<Flashcard>, private val dao: FlashcardDao) :
        RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    private val dataSet: MutableList<Flashcard> = mutableListOf()

    init {
        this.dataSet.addAll(dataSet)
    }

    class ViewHolder(view: View) :RecyclerView.ViewHolder(view) {
        val flashcardTitle: TextView = view.findViewById(R.id.flashcard_title)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.flashcardTitle.text = item.question

        viewHolder.itemView.setOnClickListener {
            showFlashcardDialog(viewHolder.itemView.context, item.question, item.answer, position)
        }

        viewHolder.itemView.setOnLongClickListener {
            editFlashcardDialog(viewHolder.itemView.context, item.question, item.answer, position)
            true
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun addFlashcard(flashcard: Flashcard) {
        dataSet.add(flashcard)
        GlobalScope.launch {
            dao.insert(flashcard)
        }
        notifyItemInserted(dataSet.size-1)
    }

    fun removeFlashcard(position: Int, flashcard: Flashcard) {
        dataSet.removeAt(position)
        GlobalScope.launch {
            dao.delete(flashcard)
        }
    }

    fun doneFlashcard(position: Int, titleEditText: String, bodyEditText: String) {
        val item = dataSet[position]
        item.question = titleEditText
        item.answer = bodyEditText
        GlobalScope.launch {
            dao.update(item)
        }
    }

    private fun showFlashcardDialog(context: Context, question: String, answer: String, position: Int) {
        AlertDialog.Builder (context)
            .setTitle(question)
            .setMessage(answer)
            .setPositiveButton("Edit")
                { dialogInterface: DialogInterface, i: Int ->
                    editFlashcardDialog(context, question, answer, position)
                }
            .create()
            .show()
    }

    private fun editFlashcardDialog(context: Context, question: String, answer: String, position: Int) {
        val titleView = LayoutInflater.from(context).inflate(R.layout.custom_title, null)
        val bodyView = LayoutInflater.from(context).inflate(R.layout.custom_body, null)
        val titleEditText = titleView.findViewById<EditText>(R.id.custom_title)
        val bodyEditText = bodyView.findViewById<EditText>(R.id.custom_body)
        titleEditText.setText(question)
        bodyEditText.setText(answer)
        AlertDialog.Builder (context)
            .setCustomTitle(titleView)
            .setView(bodyView)
            .setPositiveButton("Done")
            { dialogInterface: DialogInterface, i: Int ->
                doneFlashcard(position, titleEditText.text.toString(), bodyEditText.text.toString())
                notifyItemChanged(position)
            }
            .setNegativeButton("Delete")
            { dialogInterface: DialogInterface, i: Int ->
                val item = dataSet[position]
                removeFlashcard(position, item)
                notifyItemRemoved(position)
            }
            .create()
            .show()
    }

}

