package cs.mad.flashcards.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.databinding.ItemFlashcardSetBinding
import cs.mad.flashcards.entities.FlashcardSet

class FlashcardSetAdapter(dataSet: List<FlashcardSet>) :
        RecyclerView.Adapter<FlashcardSetAdapter.ViewHolder>() {

    private val dataSet: MutableList<FlashcardSet> = mutableListOf()

    init {
        this.dataSet.addAll(dataSet)
    }

    class ViewHolder(bind: ItemFlashcardSetBinding) : RecyclerView.ViewHolder(bind.root) {
        val binding: ItemFlashcardSetBinding = bind
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemFlashcardSetBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        binding.root.minimumHeight = viewGroup.height / 4
        return ViewHolder(binding)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.binding.flashcardSetTitle.text = item.title
        viewHolder.binding.root.setOnClickListener {
            viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context, FlashcardSetDetailActivity::class.java))
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun addItem(it: FlashcardSet) {
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }

//    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
//        val flashcardSetTitle: TextView = view.findViewById(R.id.flashcard_set_title)
//        val root = view
//    }
//
//    // Create new views (invoked by the layout manager)
//    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
//        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard_set, viewGroup, false)
//        view.minimumHeight = viewGroup.measuredHeight / 4
//        return ViewHolder(view)
//    }
//
//    // Replace the contents of a view (invoked by the layout manager)
//    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
//        val item = dataSet[position]
//        viewHolder.flashcardSetTitle.text = item.title
//
//        viewHolder.itemView.setOnClickListener {
//            viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context, FlashcardSetDetailActivity::class.java))
//        }
//    }
//
//    override fun getItemCount(): Int {
//        // return the size of the data set
//        return dataSet.size
//    }
//
//    fun addFlashcardSet(flashcardSet: FlashcardSet) {
//        dataSet.add(flashcardSet)
//        notifyItemInserted(dataSet.size-1)
//    }
}