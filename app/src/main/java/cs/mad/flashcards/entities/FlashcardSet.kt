package cs.mad.flashcards.entities

import androidx.room.*
import com.google.gson.annotations.SerializedName


data class FlashcardSets(val title: String)

@Entity
data class FlashcardSet(@PrimaryKey(autoGenerate = true) val myId: Long?,
                        @SerializedName("id") val outsideId: Long?, val title: String)

@Dao
interface FlashcardSetDao {

    @Query("select * from flashcardset order by lower(title) asc")
    suspend fun getAll(): List<FlashcardSet>

    @Insert
    suspend fun insert(vararg flashcardSet: FlashcardSet)

    @Insert
    suspend fun insert(flashcardSet: List<FlashcardSet>)

    @Update
    suspend fun update(flashcardSet: FlashcardSet)

    @Delete
    suspend fun delete(flashcardSet: FlashcardSet)
    }



//fun getHardcodedFlashcardSets(): List<FlashcardSet> {
//    return mutableListOf(
//        FlashcardSet( "Set 1"),
//        FlashcardSet( "Set 2"),
//        FlashcardSet( "Set 3"),
//        FlashcardSet( "Set 4"),
//        FlashcardSet( "Set 5"),
//        FlashcardSet( "Set 6"),
//        FlashcardSet( "Set 7"),
//        FlashcardSet( "Set 8"),
//        FlashcardSet( "Set 0"),
//        FlashcardSet( "Set 10")
//    )
//}