package cs.mad.flashcards.entities

import androidx.room.*
import android.content.Context


@Database(entities = [Flashcard::class, FlashcardSet::class], version = 1)
abstract class FlashcardSetDatabase: RoomDatabase() {
    abstract fun fsDao(): FlashcardSetDao
    abstract fun fDao(): FlashcardDao

    companion object {
        @Volatile
        private var INSTANCE: FlashcardSetDatabase? = null

        fun getDatabase(context: Context): FlashcardSetDatabase {

            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        FlashcardSetDatabase::class.java,
                        "app_database"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                instance
            }
        }
    }


}

