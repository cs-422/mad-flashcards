package cs.mad.flashcards.entities

import androidx.room.*
import com.google.gson.annotations.SerializedName

data class Flashcards(
    var question: String,
    var answer: String
)

@Entity
data class Flashcard(@PrimaryKey(autoGenerate = true) val myId: Long?,
                     @SerializedName("id") val outsideId: Long?, var question: String, var answer: String)

@Dao
interface FlashcardDao {
    @Query("select * from flashcard order by lower(question) asc")
    suspend fun getAll(): List<Flashcard>

    @Insert
    suspend fun insert(vararg flashcard: Flashcard)

    @Insert
    suspend fun insert(flashcard: List<Flashcard>)

    @Update
    suspend fun update(flashcard: Flashcard)

    @Delete
    suspend fun delete(flashcard: Flashcard)
}

//fun getHardcodedFlashcards(): List<Flashcard> {
//    return listOf(
//        Flashcard("Term 1", "Def 1"),
//        Flashcard( "Term 2", "Def 2"),
//        Flashcard( "Term 3", "Def 3"),
//        Flashcard( "Term 4", "Def 4"),
//        Flashcard( "Term 5", "Def 5"),
//        Flashcard( "Term 6", "Def 6"),
//        Flashcard( "Term 7", "Def 7"),
//        Flashcard( "Term 8", "Def 8"),
//        Flashcard( "Term 9", "Def 9"),
//        Flashcard( "Term 10", "Def 10")
//    )
//}