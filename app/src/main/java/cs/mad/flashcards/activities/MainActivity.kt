package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDatabase
import kotlinx.coroutines.launch
import androidx.lifecycle.lifecycleScope
import cs.mad.flashcards.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val fsDao by lazy { FlashcardSetDatabase.getDatabase(applicationContext).fsDao() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.title = "MAD Flashcards"

        setupRecycler()
        setupAddButton()
    }

    private fun setupAddButton() {
        findViewById<Button>(R.id.create_set_button).setOnClickListener {
            val newSet = FlashcardSet(null, null, "New Set")
            lifecycleScope.launch {
                fsDao.insert(newSet)
            }
            val recycler = findViewById<RecyclerView>(R.id.flashcard_set_list)
            (recycler?.adapter as? FlashcardSetAdapter)?.let {
                it.addItem(newSet)
                recycler.smoothScrollToPosition(it.itemCount - 1)
            }
        }
    }

    private fun setupRecycler() {
        lifecycleScope.launch{
            binding.flashcardSetList.adapter = FlashcardSetAdapter(fsDao.getAll())
        }
    }

}