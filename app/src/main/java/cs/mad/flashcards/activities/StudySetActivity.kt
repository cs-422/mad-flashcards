package cs.mad.flashcards.activities

import android.app.AlertDialog
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.lifecycle.lifecycleScope
import cs.mad.flashcards.R
import cs.mad.flashcards.databinding.ActivityStudySetBinding
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSetDatabase
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*

class StudySetActivity : AppCompatActivity() {
    private lateinit var binding: ActivityStudySetBinding
    private var completeNum = 0
    private var missedNum = 0
    private var correctNum = 0
    private var cardsSize = 0
    private val fDao by lazy { FlashcardSetDatabase.getDatabase(applicationContext).fDao() }
    private var cards = mutableListOf<Flashcard>()
    private var missedList = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStudySetBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.title = "Flashcard Set Title"

        lifecycleScope.launch {
            cards = fDao.getAll().toMutableList()
            cardsSize = cards.size
            showCurrentCard()
            setupExitButton()
            setupMissedButton()
            setupSkippedButton()
            setupCorrectButton()
        }
    }

    private fun showCurrentCard() {
        val textView = findViewById<TextView>(R.id.cardView_Text)
        textView.setText(cards.first().question)

        findViewById<CardView>(R.id.cardView_flashcard).setOnClickListener {
            val textView = findViewById<TextView>(R.id.cardView_Text)
            textView.setText(cards.first().answer)
        }

        val completeTotal = findViewById<TextView>(R.id.text_complete)
        completeTotal.setText("" + completeNum + "/" + cardsSize + " Complete")
    }

    private fun setupExitButton() {
        findViewById<Button>(R.id.button_exit).setOnClickListener {
            finish()
        }
    }

    private fun setupMissedButton() {
        findViewById<Button>(R.id.button_missed).setOnClickListener {
            val missedTotal = findViewById<TextView>(R.id.text_missed)
            Collections.rotate(cards, -1)
            missedNum++
            missedList.add(cards.first().question)
            missedTotal.setText("Missed: " + missedNum)
            val current = cards.removeAt(0)
            cards.add(current)
            showCurrentCard()
        }
    }

    private fun setupSkippedButton() {
        findViewById<Button>(R.id.button_skip).setOnClickListener {
            Collections.rotate(cards, -1)
            showCurrentCard()
        }
    }

    private fun setupCorrectButton() {
        findViewById<Button>(R.id.button_correct).setOnClickListener {
            val correctTotal = findViewById<TextView>(R.id.text_correct)
            val completeTotal = findViewById<TextView>(R.id.text_complete)
            if (cards.isNotEmpty()) {
                if (missedList.contains(cards.first().question)) {
                    var ML = false
                } else {
                    correctNum++
                }
                completeNum++
                correctTotal.setText("Correct: " + correctNum)
                completeTotal.setText("" + completeNum + "/" + cardsSize + " Complete")
                cards.removeAt(cards.indexOf(cards.first()))
            } else {
                AlertDialog.Builder(this)
                    .setTitle("Congratulations!")
                    .setMessage("You have completed this deck!")
                    .setPositiveButton("Return to Flashcard Sets List")
                    { dialogInterface: DialogInterface, i: Int ->
                        finish()
                    }
                    .create()
                    .show()
            }
        }
    }
}

