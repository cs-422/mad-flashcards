package cs.mad.flashcards.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSetDatabase
import androidx.lifecycle.lifecycleScope
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import kotlinx.coroutines.launch

class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding
    private val fDao by lazy { FlashcardSetDatabase.getDatabase(applicationContext).fDao() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.title = "Flashcard Set Detail"

        setupRecycler()
        setupAddButton()

        findViewById<Button>(R.id.study_set_button).setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }

        findViewById<Button>(R.id.delete_set_button).setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    private fun setupAddButton() {
        findViewById<Button>(R.id.add_set_button).setOnClickListener {
            val newCard = Flashcard(null, null, "New Question","New Answer")
            lifecycleScope.launch {
                fDao.insert(newCard)
            }
            val recycler = findViewById<RecyclerView>(R.id.flashcard_list)
            (recycler?.adapter as? FlashcardAdapter)?.let {
                it.addFlashcard(newCard)
                recycler.smoothScrollToPosition(it.itemCount - 1)
            }
        }
    }

    fun setupRecycler() {
        lifecycleScope.launch{
            binding.flashcardList.adapter = FlashcardAdapter(fDao.getAll(), fDao)
        }
    }
}